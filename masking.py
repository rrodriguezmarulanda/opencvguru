import argparse
import numpy as np
import cv2
import imutils


# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
cv2.imshow("Original", image)
cv2.waitKey(0)


#Build the mask
mask = np.zeros((image.shape[:2]),dtype="uint8")
rectangle = cv2.rectangle(mask,(20,50),(150,500),255,-1)
cv2.imshow("Mask", mask)
cv2.waitKey(0)


#Appling Mask
masked = cv2.bitwise_and(image,image,mask=mask)
cv2.imshow("Masked Image", masked)
cv2.waitKey(0)


#circle Mask

mask = np.zeros((image.shape[:2]),dtype="uint8")
h,w = image.shape[:2]
mask = cv2.circle(mask,(w//2,h//2),300,255,-1)
masked = cv2.bitwise_or(image,image,mask=mask)
cv2.imshow("Mask", mask)
cv2.imshow("Masked Image", masked)
cv2.waitKey(0)
