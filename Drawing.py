import numpy as np
import cv2

#Create out camvas from 0 with 300x300 3 channels

canvas = np.zeros((300,300,3),dtype="uint8")
#print(canvas)
green = (0,255,0)
cv2.line(canvas,(0,0),(300,300),green)
cv2.imshow("Canvas", canvas)
cv2.waitKey(0)

red = (0,0,255)
cv2.line(canvas,(75,150),(225,150),red,2,lineType=4)
#cv2.rectangle(canvas,(20,20),(60,100),red,1)
#cv2.circle(canvas,(40,40),20,(255,0,0),-3)

canvas = np.zeros((300,300,3),dtype="uint8")

for r in range(0,175,10):
	cv2.circle(canvas,(150,150),r,(r*15,r*2,r*5))

#cv2.putText(canvas,"Prueba",(75,150),cv2.FONT_HERSHEY_SIMPLEX,0.75,green)
cv2.imshow("Canvas", canvas)
cv2.waitKey(0)

#Aleatory Circles

for i in range(0,25):
	radio = np.random.randint(5,high=200)
	color = np.random.randint(0,high=256,size=(3,)).tolist()
	pt = np.random.randint(0,high=256,size=(2,))

	cv2.circle(canvas,tuple(pt),radio,color, -1)

cv2.imshow("Canvas", canvas)
cv2.waitKey(0)