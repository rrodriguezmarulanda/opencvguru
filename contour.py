import argparse
import numpy as np
import cv2
import imutils


# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("Original", image)
cv2.waitKey(0)

blurred = cv2.GaussianBlur(gray,(7,7),0)


cnts = cv2.findContours(blurred.copy(),cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
#print(cnts)
cnts = cnts[0] if imutils.is_cv2() else cnts[1]
clone = image.copy()

for c in cnts:
	M = cv2.moments(c)
	#print(M)
	cX = int(M["m10"]/M["m00"])
	cY = int(M["m01"]/M["m00"])

	cv2.circle(clone,(cX,cY),10,(0,255,255),-1)

cv2.imshow("Centroids", clone)
cv2.waitKey(0)

for (i, c) in enumerate(cnts):
	area = cv2.contourArea(c)
	perimeter = cv2.arcLength(c,True)
	print("The Area of the contour is {} and the perimeter is {}".format(area,perimeter))
	cv2.drawContours(clone,[c],-1,(255,255,255),2)
	M = cv2.moments(c)
	cX = int(M["m10"]/M["m00"])
	cY = int(M["m01"]/M["m00"])
	cv2.putText(clone,"#{}".format(i),(cX-20,cY),cv2.FONT_HERSHEY_SIMPLEX,1,(255,255,255),2)

cv2.imshow("Centroids Numerated", clone)
cv2.waitKey(0)

clone = image.copy()

for c in cnts:
	(x,y,w,h)= cv2.boundingRect(c)
	cv2.rectangle(clone,(x,y),(x+w,y+h),(255,255,255),2)

cv2.imshow("Bounding boxes", clone)
cv2.waitKey(0)
clone = image.copy()

for c in cnts:
	box = cv2.minAreaRect(c)
	print("Before: " + str(box))
	test= cv2.boxPoints(box)
	print(test)
	box = np.int0(test)
	print("After: " + str(box))
	cv2.drawContours(clone,[box],-1,(255,255,255),2)

cv2.imshow("Rotated Bounding boxes", clone)
cv2.waitKey(0)

clone = image.copy()

for c in cnts:
	((x,y),rad) = cv2.minEnclosingCircle(c)
	#print("Return: {}".format(cir))
	cv2.circle(clone,(int(x),int(y)),int(rad),(255,255,255),2)

cv2.imshow("Circle Bounding ", clone)
cv2.waitKey(0)

clone = image.copy()

for c in cnts:
	print(len(c))
	if len(c)>300:
		ell=cv2.fitEllipse(c)
		cv2.ellipse(clone,ell,(255,255,255),2)

cv2.imshow("Ellipse Bounding ", clone)
cv2.waitKey(0)