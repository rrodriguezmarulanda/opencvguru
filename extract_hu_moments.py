import cv2
from imutils import contours
import imutils

image = cv2.imread("planes.png")
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

moments = cv2.HuMoments(cv2.moments(image)).flatten()
print("ORIGINAL MOMENTS: {}".format(moments))
cv2.imshow("Image",image)
cv2.waitKey(0)

cnts = cv2.findContours(image.copy(),cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
print(cnts)
cnts = imutils.grab_contours(cnts)
#cnts = cnts[1]

for (i,c) in enumerate(cnts):
	(x,y,w,h) = cv2.boundingRect(c)
	roi = image[y:y + h, x:x+w]
	moments = cv2.HuMoments(cv2.moments(roi)).flatten()

	print("MOMENTS FOR PLANE #{}: {}". format(i+1,moments))
	cv2.imshow("ROI #{}". format(i+1), roi)
	cv2.waitKey(0)