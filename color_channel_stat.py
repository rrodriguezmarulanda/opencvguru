from scipy.spatial import distance as dist
from imutils import paths
import numpy as np
import cv2

#get list of image paths and initialize the index to store de image Name and feature vector
imagePaths = sorted(list(paths.list_images("dinos")))
index = {}


#loop over the paths
for imagePath in imagePaths:
	image = cv2.imread(imagePath)
	filename = imagePath[imagePath.rfind("/") + 1:]
	#print(filename)

	#Calculate de mean and Standard Deviation of the image 
	(mean,std) = cv2.meanStdDev(image)
	#print("Mean: {}, Std: {}".format(mean,std))
	features = np.concatenate([mean,std])
	#print(features)
	features = features.flatten()
	#print(features)
	index[filename] = features

query = cv2.imread(imagePaths[0])
cv2.imshow("Reference image",query)
keys=sorted(index.keys())

for (i,k) in enumerate(keys):
	if k == "car.jpg":
		continue

	image = cv2.imread(imagePaths[i])
	d = dist.euclidean(index["car.jpg"],index[k])

	cv2.putText(image, "%.2f" % (d), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2)
	cv2.imshow(k, image)
 
# wait for a keypress
cv2.waitKey(0)
