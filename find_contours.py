import argparse
import numpy as np
import cv2
import imutils


# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("Gray", gray)
cv2.imshow("Original", image)
cv2.waitKey(0)
blured = cv2.GaussianBlur(gray,(3,3),0)


thresh = cv2.adaptiveThreshold(blured,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,-5)
cv2.imshow("OpenCV Mean Thresh",thresh)
cv2.waitKey(0)


#Find all the contours in the image 
cnts = cv2.findContours(thresh.copy(),cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
#print(cnts)
cnts = cnts[0] if imutils.is_cv2() else cnts[1]
clone = image.copy()
cv2.drawContours(clone,cnts,-1,(0,255,0),2)
print("Contour Found : {}".format(len(cnts)))
cv2.imshow("Clone", clone)
cv2.waitKey(0)

clone = image.copy()
cv2.destroyAllWindows()

#Loop over the contours
for (i,c) in enumerate(cnts):
	print("Drawing contour # {}".format(i))
	cv2.drawContours(clone,[c],-1,(0,255,0),2)
	cv2.imshow("Single Contour", clone)
	cv2.waitKey(0)

clone = image.copy()
cv2.destroyAllWindows()

#Loop over the contours
for c in cnts:
	#print("Drawing contour # {}".format(i))
	mask = np.zeros(gray.shape, dtype="uint8")
	cv2.drawContours(mask,[c],-1,255,-1)

	cv2.imshow("Original", image)
	cv2.imshow("Mask", mask)
	cv2.imshow("Original+mask", cv2.bitwise_and(image,image,mask=mask))
	#cv2.imshow("Single Contour", clone)
	cv2.waitKey(0)

