import argparse
import numpy as np
import cv2
import imutils


# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
args = vars(ap.parse_args())
print(ap.parse_args())
print(args)

image = cv2.imread(args["image"])
cv2.imshow("Original", image)
cv2.waitKey(0)


(B,G,R)= cv2.split(image)

reds = np.ones(image.shape[:2],dtype="uint8")*10
cv2.imshow("R",R)
cv2.imshow("G",G)
cv2.imshow("B",B)
R = cv2.subtract(reds,R)
cv2.waitKey(0)

#merge
merged = cv2.merge([B,G,R])
cv2.imshow("Merged", merged)
cv2.waitKey(0)