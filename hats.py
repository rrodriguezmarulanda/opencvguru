import argparse
import numpy as np
import cv2

import imutils


# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


"""
HSV = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
(H,S,V) = cv2.split(HSV)
gray = V
"""



cv2.imshow("Original", image)
cv2.waitKey(0)

rectkernel = cv2.getStructuringElement(cv2.MORPH_RECT, (30,10))
blackhat = cv2.morphologyEx(gray,cv2.MORPH_BLACKHAT,rectkernel)

tophat = cv2.morphologyEx(gray,cv2.MORPH_TOPHAT,rectkernel)

#cv2.imshow("Original", image)
#cv2.imshow("tophat",tophat)
cv2.imshow("blackhat",blackhat)
cv2.imwrite("blackhat.jpg",blackhat)
cv2.waitKey(0)