import cv2

image = cv2.imread("pent.jpg")
cv2.imshow("Original", image)


# 50% of the image from the center
print(image.shape)
h,w = image.shape[:2]
cv2.waitKey(0)

(cy,cx) = (h//2,w//2)

cropImage = image[int(cy-(cy/2)):int(cy+(cy/2)),int(cx-(cx/2)):int(cx+(cx/2))]
cv2.imshow("Croped", cropImage)
cv2.waitKey(0)


