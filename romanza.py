import argparse
import numpy as np
import cv2
import imutils


# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
print(image)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
print(image.shape)

#nota con thres_bin_inv todo lo mayor lo pasa a negro
(T, thresInv)= cv2.threshold(gray,0,255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)
print("Threshold Value: "+ str(T))
cv2.imshow("Otsu",thresInv)
cv2.waitKey(0)
print(thresInv.shape)
print(thresInv)

cnts = cv2.findContours(thresInv.copy(),cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
#print(cnts)
cnts = cnts[0] if imutils.is_cv2() else cnts[1]
clone = image.copy()

#zeros = np.zeros((thresInv.shape[0],thresInv.shape[0],3),dtype="uint8")
#print(zeros)

B = np.ones(thresInv.shape, dtype="uint8")*60
G = np.ones(thresInv.shape, dtype="uint8")*17
R = np.ones(thresInv.shape, dtype="uint8")*168
merged = cv2.merge([B,G,R])
cv2.imshow("Merged", merged)
cv2.waitKey(0)

for c in cnts:
	area = cv2.contourArea(c)

	if area > 73:

		if area == 1459.5 or  area == 1460.5 or area == 292.5:
			cv2.drawContours(merged,[c],-1,(60,17,168),-1)
		else:
			cv2.drawContours(merged,[c],-1,(255,255,255),-1)
		#print(cv2.contourArea(c))



cv2.imshow("Cnts", merged)
cv2.waitKey(0)
cv2.imwrite("Romanza.png", merged)

