import argparse
import numpy as np
import cv2
import imutils


# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
HSV = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
cv2.imshow("Original", HSV)
cv2.waitKey(0)


(H,S,V) = cv2.split(HSV)
cv2.imshow("H",H)
cv2.imshow("S",S)
cv2.imshow("V",V)
cv2.waitKey(0)

LAB = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
cv2.imshow("Original", LAB)
cv2.waitKey(0)


(L,A,B) = cv2.split(LAB)
cv2.imshow("L",L)
cv2.imshow("A",A)
cv2.imshow("B",B)
cv2.waitKey(0)
