import cv2
import argparse
import numpy as np

#Construct the argument parser
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image",required=True, help="Path to the Image")
ap.add_argument("-o", "--output",required=True, help="Path save the Image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
print(image.shape)
print("Widht: %d pixels" % (image.shape[1]))   
print("height: %d pixels" % (image.shape[0]))
print("channels: %d" % (image.shape[2])) 


cv2.imshow("Image", image)
cv2.waitKey(0)

cv2.imwrite(args["output"], image)