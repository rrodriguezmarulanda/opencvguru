import argparse
import numpy as np
import cv2
import imutils
from matplotlib import pyplot as plt


# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
ap.add_argument("-t","--type",required= True, help="color or gray image?")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("Original", image)
cv2.waitKey(0)

def plot_histogram(image,title,mask=None):
	chans = cv2.split(image)
	colors = ("b","g","r")
	plt.figure()
	plt.title(title)
	plt.xlabel("Bins")
	plt.ylabel("# of Pixels")

	for (chan,color) in zip(chans,colors):
		hist = cv2.calcHist([chan],[0],mask,[256],[0,256])
		plt.plot(hist,color=color)
		plt.xlim([0,256])
	plt.show()

if args["type"] == "gray":
	hist = cv2.calcHist(gray,[0],mask=None, histSize=[256],ranges=[0,256])

	plt.figure()
	plt.title("GrayScale Histogram")
	plt.xlabel("Bins")
	plt.ylabel("# of Pixels")
	plt.plot(hist)
	plt.xlim([0,256])

	hist /= hist.sum()

	plt.figure()
	plt.title("GrayScale Histogram (Normalized)")
	plt.xlabel("Bins")
	plt.ylabel("# of Pixels")
	plt.plot(hist)
	plt.xlim([0,256])
	plt.show()

if args["type"] == "color":
	chans = cv2.split(image)
	colors = ("b","g","r")
	plt.figure()
	plt.title("Color Histogram")
	plt.xlabel("Bins")
	plt.ylabel("# of Pixels")

	for (chan,color) in zip(chans,colors):
		hist = cv2.calcHist([chan],[0],None,[256],[0,256])
		plt.plot(hist,color=color)
		plt.xlim([0,256])

	fig = plt.figure()

	ax = fig.add_subplot(131)
	hist = cv2.calcHist([chans[0],chans[1]],[0,1],None,[32,32],[0,256,0,256])
	p = ax.imshow(hist,interpolation="nearest")
	ax.set_title("2D Color Histogram for G and B")
	plt.colorbar(p)

	ax = fig.add_subplot(132)
	hist = cv2.calcHist([chans[1],chans[2]],[0,1],None,[32,32],[0,256,0,256])
	p = ax.imshow(hist,interpolation="nearest")
	ax.set_title("2D Color Histogram for G and R")
	plt.colorbar(p)

	ax = fig.add_subplot(133)
	hist = cv2.calcHist([chans[0],chans[2]],[0,1],None,[32,32],[0,256,0,256])
	p = ax.imshow(hist,interpolation="nearest")
	ax.set_title("2D Color Histogram for B and R")
	plt.colorbar(p)

	print("2D histogram shape: {}, with {} values".format(hist.shape,hist.flatten().shape[0]))
	plt.show()

if args["type"] == "equalize":
	eq = cv2.equalizeHist(gray)

	cv2.imshow("Original", image)
	cv2.imshow("Equalized", eq)
	cv2.waitKey(0)



if args["type"] == "mask":
	print(image.shape)
	#plot_histogram(image,"taxi")

	mask = np.zeros(image.shape[:2],dtype="uint8")
	cv2.rectangle(mask,(200,200),(1000,600),255,-1)
	cv2.imshow("mask", mask)
	cv2.waitKey(0)

	masked = cv2.bitwise_and(image,image ,mask=mask)
	cv2.imshow("applied mask", masked)
	cv2.waitKey(0)

	plot_histogram(image,"taxi",mask=mask)