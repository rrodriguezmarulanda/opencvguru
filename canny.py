import argparse
import numpy as np
import cv2
import imutils


# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(gray,(5,5),0)

cv2.imshow("Original", image)
cv2.imshow("Blurred", blurred)
cv2.waitKey(0)

#Compute the canny edge detector 
wide = cv2.Canny(blurred,10,200)
mid = cv2.Canny(blurred,30,150)
tight = cv2.Canny(blurred,200,240)
auto = imutils.auto_canny(blurred)

cv2.imshow("wide", wide)
cv2.imshow("mid", mid)
cv2.imshow("tight", tight)
cv2.imshow("Auto", auto)
cv2.waitKey(0)
