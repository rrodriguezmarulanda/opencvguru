import cv2
import argparse

#Construct the argument parser
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image",required=True, help="Path to the Image")
ap.add_argument("-o", "--output",required=False, help="Path save the Image")
args = vars(ap.parse_args())

#Image Characteristicts
image = cv2.imread(args["image"])
(h,w) = image.shape[:2]
print("Height : %d" % h)
print("Weight : %d" % w)

# get the color of the pixel (0,0)
(b,g,r) = image[0,1]
print("Pixel at (0,0) - Red: {}, Green: {}, Blue: {}".format(r,g,b))
#print(image)

image[0,0] = (0,0,255)
(b,g,r) = image[0,1]
print("Pixel at (0,0) - Red: {}, Green: {}, Blue: {}".format(r,g,b))
#print(image)


cv2.imshow("Image", image)
cv2.waitKey(0)

#Find the center of the image 

(cx,cy) = (w//2,h//2)

print(cx,cy)
cutProp = 0.83

tl = image[int(cy-(cy*cutProp)):int(cy+(cy*cutProp)),int(cx-(cx*cutProp)):int(cx+(cx*(cutProp-0.07)))]
print("Shape of tl: " + str(tl.shape))
cv2.imshow("Top Left", tl)
cv2.waitKey(0)

image[int(cy-(cy*cutProp)):int(cy+(cy*cutProp)),int(cx-(cx*cutProp)):int(cx+(cx*(cutProp-0.07)))] = (255,0,0)
cv2.imshow("Updated", image)
cv2.waitKey(0)
