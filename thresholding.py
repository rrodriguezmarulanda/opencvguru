import argparse
import numpy as np
import cv2
import imutils
from skimage.filters import threshold_local


# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
blured = cv2.GaussianBlur(gray,(5,5),0)
cv2.imshow("Original", blured)
cv2.waitKey(0)



#nota con thres_bin_inv todo lo mayor lo pasa a negro
(T, thresInv)= cv2.threshold(blured,220,255,cv2.THRESH_BINARY_INV)
cv2.imshow("Inverse",thresInv)
cv2.waitKey(0)

#nota con thres_binary todo lo mayor lo pasa a Blanco
(T, thres)= cv2.threshold(blured,220,255,cv2.THRESH_BINARY)
cv2.imshow("Inverse",thres)
cv2.waitKey(0)

#Logo con fondo negro
cv2.imshow("Output",cv2.bitwise_and(image,image,mask=thresInv))
cv2.waitKey(0)

#nota con thres_bin_inv todo lo mayor lo pasa a negro
(T, thresInv)= cv2.threshold(blured,0,255,cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)
print("Threshold Value: "+ str(T))
cv2.imshow("Otsu",thresInv)
cv2.waitKey(0)

thresh = cv2.adaptiveThreshold(blured,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV,11,-5)
cv2.imshow("OpenCV Mean Thresh",thresh)
cv2.waitKey(0)

T = threshold_local(blured,13,offset=0,method="gaussian")
thresh =(blured < T).astype("uint8")*255
cv2.imshow("Skimage",thresh)
cv2.waitKey(0)