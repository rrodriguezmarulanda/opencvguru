import argparse
import numpy as np
import cv2
import imutils


# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
cv2.imshow("Original", image)
print(image.shape)
cv2.waitKey(0)


# to translate add the amount of pixel that you want to translate to the M matrix
M = np.float32([[1,0,25],[0,1,50]])
shifted = cv2.warpAffine(image,M,(image.shape[1],image.shape[0]))
cv2.imshow("Shifted", shifted)
cv2.waitKey(0)


M = np.float32([[1,0,-50],[0,1,-90]])
shifted = cv2.warpAffine(image,M,(image.shape[1],image.shape[0]))
cv2.imshow("Shifted", shifted)
cv2.waitKey(0)

#Shifted with imutils
#imShifted = imutils.translate(image,100,100)
imShifted = imutils.rotate(image,20, (100,50),0.3)
cv2.imshow("imShifted", imShifted)
cv2.waitKey(0)

#imShifted = imutils.translate(image,100,100)
imResize = imutils.resize(image,width=480, inter=cv2.INTER_LINEAR)
cv2.imshow("imResize", imResize)
cv2.waitKey(0)

methods = [
	("cv2.INTER_NEAREST", cv2.INTER_NEAREST),
	("cv2.INTER_LINEAR", cv2.INTER_LINEAR),
	("cv2.INTER_AREA", cv2.INTER_AREA),
	("cv2.INTER_CUBIC", cv2.INTER_CUBIC),
	("cv2.INTER_LANCZOS4", cv2.INTER_LANCZOS4)]
 
# loop over the interpolation methods
for (name, method) in methods:
	# increase the size of the image by 3x using the current interpolation
	# method
	resized = imutils.resize(image, width=image.shape[1] * 3, inter=method)
	cv2.imshow("Method: {}".format(name), resized)
	cv2.waitKey(0)