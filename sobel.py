import argparse
import numpy as np
import cv2
import imutils


# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("Original", image)
cv2.waitKey(0)

#Compute sobel gradients
gx = cv2.Scharr(gray,ddepth=cv2.CV_64F,dx=1,dy=0)
print("Gx :{}".format(gx))
gy = cv2.Scharr(gray,ddepth=cv2.CV_64F,dx=0,dy=1)
print("Gy :{}".format(gy))


#convert to uint8
gx = cv2.convertScaleAbs(gx)
gy = cv2.convertScaleAbs(gy)
print("Gx :{}".format(gx))
print("Gy :{}".format(gy))

sobelcombined = cv2.addWeighted(gx,0.5,gy,0.5,0)

cv2.imshow("SobelX", gx)
cv2.imshow("SobelY", gy)
cv2.imshow("SobelComb", sobelcombined)
cv2.waitKey(0)
