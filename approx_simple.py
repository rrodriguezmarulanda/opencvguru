import argparse
import numpy as np
import cv2
import imutils


# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
print(image.shape)
#image = image[300:1000,60:800]
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("Original", image)
cv2.waitKey(0)


#nota con thres_bin_inv todo lo mayor lo pasa a negro
(T, thresInv)= cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)
print("Threshold Value: "+ str(T))
cv2.imshow("Otsu",thresInv)
cv2.waitKey(0)




cnts = cv2.findContours(thresInv.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
cnts = cnts[1]
print(len(cnts))

for c in cnts:
	peri= cv2.arcLength(c,True)
	approx = cv2.approxPolyDP(c,0.01*peri,True)
	(x,y,w,h)= cv2.boundingRect(c)
	print(len(approx))

	area = cv2.contourArea(c)
	aspectRatio = w/float(h)
	#Find Convex Hull
	hull = cv2.convexHull(c)
	hullArea = cv2.contourArea(hull)
	solidity = area/float(hullArea)

	char = "?"

	if len(approx) == 6:
		char= "L-Pieze"

	if aspectRatio > 0.97 and aspectRatio < 1.03 and len(approx) == 4:
		char = "Squared"

	if aspectRatio > 1.07 and len(approx) == 4:
		char = "Rectangle"

	if len(approx) == 8:
		char = "ZT-Pieze"

	cv2.drawContours(image,[c],-1,(0,0,255),1)
	cv2.putText(image,"{}".format(char),(x,y-10),cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,0,255),1)

cv2.imshow("Original", image)
cv2.waitKey(0)