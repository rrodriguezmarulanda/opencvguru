import argparse
import numpy as np
import cv2
import imutils


# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
print(image.shape)
#image = image[300:1000,60:800]
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("Original", image)
cv2.waitKey(0)
#blured = cv2.GaussianBlur(gray,(5,5),0)
thresInv = cv2.Canny(gray,75,200)

"""#nota con thres_bin_inv todo lo mayor lo pasa a negro
(T, thresInv)= cv2.threshold(gray,0,255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)
print("Threshold Value: "+ str(T))
cv2.imshow("Otsu",thresInv)
cv2.waitKey(0)
"""

cnts = cv2.findContours(thresInv.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
cnts = cnts[1]
print(len(cnts))
cnts = sorted(cnts,key=cv2.contourArea, reverse=True)[:7]
print(len(cnts))

for c in cnts:
	peri= cv2.arcLength(c,True)
	approx = cv2.approxPolyDP(c,0.01*peri,True)
	cv2.drawContours(image,[approx],-1,(0,0,255),2)
	#print(approx)
	cv2.imshow("Original", image)
	cv2.waitKey(0)

