import argparse
import numpy as np
import cv2
import imutils


# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
print(image.shape)
#image = image[300:1000,60:800]
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("Original", image)
cv2.waitKey(0)


#nota con thres_bin_inv todo lo mayor lo pasa a negro
(T, thresInv)= cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)
print("Threshold Value: "+ str(T))
cv2.imshow("Otsu",thresInv)
cv2.waitKey(0)

cnts = cv2.findContours(thresInv.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
cnts = cnts[1]
print(len(cnts))

clone = image.copy()

for (i,c) in enumerate(cnts):
	#Find the area of the contour
	area = cv2.contourArea(c)
	(x,y,w,h)= cv2.boundingRect(c)

	#Find Convex Hull
	hull = cv2.convexHull(c)
	hullArea = cv2.contourArea(hull)
	solidity = area/float(hullArea)

	aspectRatio = w/float(h)

	extend = area/float(w*h)

	dif = area - hullArea
	
	#cv2.rectangle(image,(x,y),(x+w,y+h),(255,0,0),2)
	char = "?"

	

	if solidity >0.19:
		char = "O"

	if solidity < 0.15:
		char = "X"

	if aspectRatio > 0.97 and aspectRatio < 1.03:
		char = "Squared"

	if aspectRatio > 2 :
		char = "Rectangle"


	#if char != "?":
	cv2.drawContours(image,[c],-1,(0,0,255),1)
	cv2.putText(image,"{}".format(char),(x,y-10),cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,0,255),1)

	print("Contour #{}, solidity: {},AspectRatio: {}, Extend: {},dif:{}" .format(i,solidity,aspectRatio,extend,dif))



cv2.imshow("Image",image)
cv2.waitKey(0)

