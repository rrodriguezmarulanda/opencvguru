from skimage.filters import threshold_local
from skimage import measure
import numpy as np
import cv2
import argparse

# get the arguments 
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required= True, help="Path tho the image")
args = vars(ap.parse_args())

plate = cv2.imread(args["image"])
gray = cv2.cvtColor(plate, cv2.COLOR_BGR2GRAY)
V = cv2.split(cv2.cvtColor(plate, cv2.COLOR_BGR2HSV))[2]

cv2.imshow("Original", plate)
cv2.waitKey(0)

#nota con thres_bin_inv todo lo mayor lo pasa a negro
(T, thresInv)= cv2.threshold(V,0,255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)
print("Threshold Value: "+ str(T))
Tr = threshold_local(V,33,offset=1,method="gaussian")
thresh = (V < Tr).astype("uint8")*255

cv2.imshow("Otsu",thresInv)
cv2.imshow("hsv",thresh)

cv2.waitKey(0)

#Component analysis computation
labels = measure.label(thresInv,neighbors=8,background =0)
mask = np.zeros(thresInv.shape,dtype="uint8")
print("[INFO] found {} blobs".format(len(np.unique(labels))))

for (i, label) in enumerate(np.unique(labels)):
	if label ==0:
		print("[INFO] label:0 (background)")
		continue

	print("[INFO] label:{} (foreground)".format(i))
	labelMask = np.zeros(thresInv.shape, dtype="uint8")
	labelMask[labels == label] = 255
	numpixels = cv2.countNonZero(labelMask)

	if numpixels > 250 and numpixels < 800:
		mask = cv2.add(mask,labelMask)

	print("Numpixels : {}".format(numpixels))
	#cv2.imshow("labels",labelMask)
	#cv2.waitKey(0)

cv2.imshow("Large blobs",mask)
cv2.waitKey(0)